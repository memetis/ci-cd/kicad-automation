#!/bin/sh
# Echo commands and exit on error
set -x
set -e

# # Change to the project directory
# cd "$CI_PROJECT_DIR/${KICAD_PROJECT}"
# echo "Changed to project directory: $(pwd)"

# Remove old 'QM' directory if it exists
if test -d "QM"; then
  echo "Directory 'QM' exists"
  echo "Removing old 'QM' directory"
  rm -rf "QM"
  echo "Old 'QM' directory removed"
fi

# Create new 'QM' directory
echo "Creating new 'QM' directory"
mkdir -p "QM"
if test -d "QM"; then
  echo "Directory 'QM' created"
else
  echo "Failed to create directory 'QM'"
  exit 1
fi

cd "QM"
echo "Changed to project directory: $(pwd)"

# Remove old 'Gerber and PCB Datasheet' directory if it exists
if test -d "${KICAD_PROJECT} Gerber and PCB Datasheet"; then
  echo "Directory 'Gerber and PCB Datasheet' exists"
  echo "Removing old 'Gerber and PCB Datasheet' directory"
  rm -rf "${KICAD_PROJECT} Gerber and PCB Datasheet"
  echo "Old 'Gerber and PCB Datasheet' directory removed"
fi

# Create new 'Gerber and PCB Datasheet' directory
echo "Creating new 'Gerber and PCB Datasheet' directory"
mkdir -p "${KICAD_PROJECT} Gerber and PCB Datasheet"
if test -d "${KICAD_PROJECT} Gerber and PCB Datasheet"; then
  echo "Directory 'Gerber and PCB Datasheet' created"
else
  echo "Failed to create directory 'Gerber and PCB Datasheet'"
  exit 1
fi

# Change to the project directory
cd "${KICAD_PROJECT} Gerber and PCB Datasheet"
echo "Changed to project directory: $(pwd)"

# Remove old 'Assembly' directory if it exists
if test -d "Assembly"; then
  echo "Directory 'Assembly' exists"
  echo "Removing old 'Assembly' directory"
  rm -rf "Assembly"
  echo "Old 'Assembly' directory removed"
fi
# Create new 'fab' directory
echo "Creating new 'Assembly' directory"
mkdir -p "Assembly"
if test -d "Assembly"; then
  echo "Directory 'Assembly' created"
else
  echo "Failed to create directory 'Assembly'"
  exit 1
fi

