#!/bin/sh
# Echo commands and exit on error
set -x
set -e

cd "$CI_PROJECT_DIR/${KICAD_PROJECT}"
echo "Changed to project directory: $(pwd)"

# Find and print pcb layout and create gerbers and drill files
if [ -n "$(find "." -maxdepth 1 -name '*.kicad_pcb' -print -quit)" ]; then
    if [ -n "$(find "." -maxdepth 1 -name '*.json' -print -quit)" ]; then
        # get .json file name
        json_file=$(find . -name '*.json' | head -n 1)
        json_file=$(basename $json_file)
        echo "Found JSON file: $json_file"

        sed -i "s/kikit-panelize.json/$json_file/g" ../kicad-automation/export/config.kiplot.yaml

        kibot -c ../kicad-automation/export/config.kiplot.yaml -d "../QM/${KICAD_PROJECT} Panel" panelize
        echo "Panelized ${KICAD_PROJECT}"
    else
        echo "No .json files found in the project directory"
    fi
else
  echo "No .kicad_pcb files found in the project directory"
  exit 1
fi

