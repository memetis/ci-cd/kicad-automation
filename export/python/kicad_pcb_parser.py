import json
import logging
import re
from collections import namedtuple

logger = logging.getLogger(__name__)


class ProParser:
    def __init__(self, project: str) -> None:
        self._pcb = project
        self._text_file = None

        with open(project, "r", encoding="utf-8") as f:
            self._text_file = f.read()

    def get_constraints(self) -> dict:
        parsed_data = json.loads(self._text_file)

        rules = parsed_data['board']['design_settings']['rules']

        try:
            nets = parsed_data['board']['net_settings']['classes']
        except KeyError:
            nets = None

        clearance = 0
        track_width = 0
        via_drill = 10000000000

        if nets:
            for netclass in nets:
                clearance = (float(netclass['clearance']) if
                             float(netclass['clearance']) > clearance else
                             clearance)
                track_width = (float(netclass['track_width']) if
                               float(netclass['track_width']) > track_width else
                               track_width)
                via_drill = (float(netclass['via_drill']) if
                             float(netclass['via_drill']) < via_drill else
                             via_drill)

        clearance = (float(rules['min_clearance']) if
                     float(rules['min_clearance']) > clearance else clearance)
        track_width = (float(rules['min_track_width']) if
                       float(rules['min_track_width']) > track_width else
                       track_width)
        min_npth_drill = rules["min_through_hole_diameter"]
        min_pth_drill = (via_drill if via_drill < min_npth_drill else
                         min_npth_drill)

        return clearance, track_width, min_npth_drill, min_pth_drill

    @ property
    def constraints(self) -> dict:
        return self.get_constraints()


class PCBParser:
    def __init__(self, pcb: str) -> None:
        self._pcb = pcb
        self._text_file = None

        with open(pcb, "r", encoding="utf-8") as f:
            self._text_file = f.read()

    def _find_match(self, pattern: str) -> re.Match:
        return re.search(pattern, self._text_file)

    def _layers(self) -> dict:
        layers_section = re.search(
            r'\(layers\s*\((.*?)\)\s*\)', self._text_file, re.DOTALL).group(1)

        layers = {}
        layer_pattern = re.compile(r'\(\s*(\d+)\s+"(.*?)"\s+(\w+)\s*(.*?)\)')
        for match in layer_pattern.findall(layers_section):
            layer_num, layer_name, layer_type, layer_description = match
            layers[int(layer_num)] = {
                "name": layer_name,
                "type": layer_type,
                "description": layer_description.strip('"') if layer_description else ""
            }

        return layers

    def _stackup(self) -> dict:
        # Regex pattern to find and extract stackup section and layers
        pattern = r'\(stackup((?:\s+\(layer\s+".*?"(?:\s+\(.*?\))*\s*\))+)\s+\)'

        # Find all stackup sections and layers using the pattern
        stackup_match = re.search(pattern, self._text_file, re.DOTALL)

        if stackup_match:
            stackup_content = stackup_match.group(1)

            # Regex pattern to parse individual layer details
            layer_pattern = r'\(layer\s+"(.*?)"\s+\((.*?)\)\s*\)'
            surface_finish_pattern = r'\(copper_finish\s+"(.*?)"\)'

            # Find all layers in the stackup content
            layers = re.findall(layer_pattern, stackup_content, re.DOTALL)
            finish = re.search(surface_finish_pattern,
                               stackup_content, re.DOTALL).group(1)
            stackup_dict = {}

            for layer in layers:
                layer_name = layer[0].strip()
                layer_attributes = layer[1].strip().split('\n')

                layer_info = {'type': None}

                for attribute in layer_attributes:
                    attribute = attribute.strip()
                    if attribute.startswith('(') and attribute.endswith(')'):
                        attribute = attribute[1:-1]
                        key, value = attribute.split(maxsplit=1)
                        value = value.strip('"')
                        layer_info[key] = value
                    elif attribute.startswith('('):
                        attribute = attribute[1:]
                        key, value = attribute.split(maxsplit=1)
                        value = value.strip('"')
                        layer_info[key] = value
                    elif attribute.endswith(')'):
                        attribute = attribute[:-1]
                        key, value = attribute.split(maxsplit=1)
                        value = value.strip('"')
                        layer_info[key] = value
                    else:
                        key, value = attribute.split(maxsplit=1)
                        value = value.strip('"')
                        layer_info[key] = value

                stackup_dict[layer_name] = layer_info

            stackup_dict["copper_finish"] = finish
            return stackup_dict

        else:
            logger.error("Stackup section not found in the input content.")
            return {}

    def _dimensions(self) -> dict:
        # Regular expression to find gr_line sections with Edge.Cuts layer and start/end coordinates
        pattern = r'\(gr_line\s+[\s\S]*?\(start ([\d.]+) ([\d.]+)\)[\s\S]*?\(end ([\d.]+) ([\d.]+)\)[\s\S]*?\(layer "Edge.Cuts"\)'

        # Initialize variables for min/max values
        min_x = float('inf')
        max_x = float('-inf')
        min_y = float('inf')
        max_y = float('-inf')

        # Find all gr_line sections with Edge.Cuts layer
        matches = re.findall(pattern, self._text_file)

        # Process each match to find min/max values
        for match in matches:
            start_x = float(match[0])
            start_y = float(match[1])
            end_x = float(match[2])
            end_y = float(match[3])

            # Update min/max values for start and end coordinates
            if start_x < min_x:
                min_x = start_x
            if start_x > max_x:
                max_x = start_x
            if start_y < min_y:
                min_y = start_y
            if start_y > max_y:
                max_y = start_y

            if end_x < min_x:
                min_x = end_x
            if end_x > max_x:
                max_x = end_x
            if end_y < min_y:
                min_y = end_y
            if end_y > max_y:
                max_y = end_y

        # Calculate width and height
        width = max_x - min_x
        height = max_y - min_y

        return {"width": width, "height": height}

    @ property
    def revision(self) -> float:
        revision = float(self._find_match(r'\(rev "([^"]+)"\)').group(1))
        return revision

    @ property
    def title(self) -> str:
        title = self._find_match(r'\(title "([^"]+)"\)').group(1)
        return title

    @ property
    def date(self) -> str:
        date = self._find_match(r'\(date "([^"]+)"\)').group(1)
        return date

    @ property
    def company(self) -> str:
        company = self._find_match(r'\(company "([^"]+)"\)').group(1)
        return company

    @ property
    def layers(self) -> dict:
        layers = self._layers()
        return layers

    @ property
    def stackup(self) -> dict:
        stackup = self._stackup()
        return stackup

    @ property
    def thickness(self) -> float:
        thickness = float(
            self._find_match(r'\(thickness (\d+\.\d+)\)').group(1))
        return thickness

    @ property
    def height(self) -> float:
        return self._dimensions()["height"]

    @ property
    def width(self) -> float:
        return self._dimensions()["width"]

    @ property
    def copper_layers(self) -> int:
        count = 0
        for attribute in self.stackup.values():
            if 'type' in attribute and attribute['type'] == 'copper':
                count += 1

        return count
