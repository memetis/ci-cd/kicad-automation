import os
import sys

from docx import Document
from docx.shared import RGBColor

from kicad_pcb_parser import PCBParser, ProParser

file_path = None
panel_file_path = None
json_filepath = None
pcb_type = None


def find_file_path(directory, extension):
    FOUND = False
    file_path = None

    print(f"Searching for {extension} file in directory: {directory}")
    for root, dirs, files in os.walk(directory):
        print(f"Exisiting files in directory: {files}")
        for file in files:
            print(f"Checking file: {file}")
            if file.endswith(extension):
                file_path = os.path.join(root, file)
                print(f"Found {extension} file: {file_path}")
                FOUND = True
                break
        if FOUND:
            break

    return file_path


json_filepath = find_file_path(f"{sys.argv[1]}", '.json')

if json_filepath:
    panel_file_path = find_file_path(f"QM/{sys.argv[1]} Panel", '.kicad_pcb')
    panel_parser = PCBParser(panel_file_path)

file_path = find_file_path(f"{sys.argv[1]}", '.kicad_pcb')
project_file_path = find_file_path(f"{sys.argv[1]}", '.kicad_pro')

if file_path is None:
    print("Error: No .kicad_pcb file found in the specified directory.")
    exit()

parser = PCBParser(file_path)
project_parser = ProParser(project_file_path)

# Get the directory path where this script is located
script_directory = os.path.dirname(os.path.abspath(__file__))
print(f"Script directory: {script_directory}")

# List files in the current script directory
files_in_directory = os.listdir(script_directory)
print(f"Files in script directory: {files_in_directory}")

copper_layers = parser.copper_layers

# Find the first .docx file in the script directory
if parser.stackup['dielectric 1']['material'] == "Polyimide":
    doc_file = [
        file for file in files_in_directory if file.lower().endswith('.docx')
        and "FPC" in file]
    pcb_type = "FPC"
elif copper_layers > 2:
    doc_file = [
        file for file in files_in_directory if file.lower().endswith('.docx')
        and "4-Layers" in file]
    pcb_type = "4L"
else:
    doc_file = [
        file for file in files_in_directory if file.lower().endswith('.docx')
        and "2-Layers" in file]
    pcb_type = "2L"

print(f"Found .docx files: {doc_file}")

if len(doc_file) == 0:
    print("Error: No .docx file found in the script directory.")
    exit()

docx_filename = doc_file[0]
docx_path = os.path.join(script_directory, docx_filename)

# Load the found .docx file
doc = Document(docx_path)

doc.core_properties.title = f"{sys.argv[1]}"


def find_image(file, text):
    alt_text_dict = {}

    # Iterate through each paragraph in the document
    for paragraph in file.paragraphs:
        for run in paragraph.runs:
            # Check if the run contains an inline shape
            if run.element.xml.startswith('<w:drawing'):
                inline_shape = run._element.find(
                    './/wp:docPr', namespaces=file.part.target._partns)
                alt_text = inline_shape.get('descr')
                if alt_text:
                    alt_text_dict[run] = alt_text

    # Iterate through each inline shape in the document
    for inline_shape in file.inline_shapes:
        alt_text = inline_shape._inline.graphic.graphicData.pic.nvPicPr.cNvPr.get(
            'descr')
        if alt_text:
            alt_text_dict[inline_shape] = alt_text

    for item, alt_text in alt_text_dict.items():
        if alt_text == text:
            return item  # Return the inline shape or run object

    return None  # Return None if image with alt text is not found


def edit_table_field(file, layer_name, new_value, footer_mode=False):
    # Iterate through all tables in the document (assuming there's only one table in your case)
    for table in file.tables:
        for row in table.rows:
            if footer_mode:
                for cell in row.cells:
                    if layer_name in cell.text.strip():
                        alter_text(cell, layer_name, str(
                            new_value), replace=True)
            else:
                try:
                    # Assuming 'Layer' column is in the first cell of each row
                    cell_layer = row.cells[2].text.strip()
                    if cell_layer == str(layer_name):
                        row.cells[1].text = str(new_value)

                except IndexError:
                    pass


def alter_text(doc, old_text, new_text, replace=False):
    for paragraph in doc.paragraphs:
        if old_text in paragraph.text:
            original_size = None
            for run in paragraph.runs:
                if old_text in run.text:
                    original_size = run.font.size
                    break

            if replace:
                paragraph.text = paragraph.text.replace(old_text, new_text)
            else:
                paragraph.text += new_text

            for run in paragraph.runs:
                run.font.color.rgb = RGBColor(0, 0, 0)
                if original_size:
                    run.font.size = original_size


def replace_image(file, alt_text, new_image_path, text_to_find):
    # Find the image by alt text
    image_to_replace = find_image(file, alt_text)
    if image_to_replace:
        # Get the width and height of the existing image
        if isinstance(image_to_replace, str):  # If it's a run (text), skip
            return
        width = image_to_replace._inline.extent.cx
        height = image_to_replace._inline.extent.cy

        # Remove the existing image
        parent = image_to_replace._inline
        parent.getparent().remove(parent)

        # Create a new inline shape for the new image with the same size
        for i, paragraph in enumerate(doc.paragraphs):
            if text_to_find in paragraph.text:
                # Insert image above this paragraph
                new_run = doc.paragraphs[i - 1].add_run()
                new_run.add_picture(new_image_path, width=width, height=height)

        print(f"Image with alt text '{alt_text}' replaced successfully.")
    else:
        print(f"Image with alt text '{alt_text}' not found.")


# Access the headers
for section in doc.sections:
    header = section.header
    alter_text(header, "{ENTER PROJECT NAME}", f"{sys.argv[1]}", replace=True)

# Access the fotters
for section in doc.sections:
    footer = section.footer
    edit_table_field(footer, "{ENTER PROJECT NAME}",
                     f"{sys.argv[1]}", footer_mode=True)

alter_text(doc, "{ENTER PROJECT NAME}", f"{sys.argv[1]}", replace=True)

dimension_x = parser.width if not json_filepath else panel_parser.width
dimension_y = parser.height if not json_filepath else panel_parser.height
clearance, track_width, npth_drill, pth_drill = project_parser.constraints

alter_text(doc, "Dimension X max:", f" {dimension_x} mm")
alter_text(doc, "Dimension Y max:", f" {dimension_y} mm")

alter_text(doc, "Minimum clearance outer layers:",
           f" {clearance} mm")
alter_text(doc, "Minimum clearance inner layers:",
           f" {clearance} mm")
alter_text(doc, "Minimum track width outer layers:",
           f" {track_width} mm")
alter_text(doc, "Minimum track width inner layers:",
           f" {track_width} mm")
alter_text(doc, "Minimum drill size plated hole",
           f" {pth_drill} mm")
alter_text(doc, "Minimum drill size non-plated hole:",
           f" {npth_drill} mm")

alter_text(doc, "Copper layers:", f" {copper_layers}")
alter_text(doc, "Total thickness:", f" {parser.thickness} mm")
alter_text(doc, "Surface finish type:",
           f" {parser.stackup['copper_finish']}")

image_paths = [f"QM/{sys.argv[1]} Gerber and PCB Datasheet/Assembly/Rendering Top Side.png",
               # Paths to the new images
               f"QM/{sys.argv[1]} Gerber and PCB Datasheet/Assembly/Rendering Bottom Side.png"]

replace_image(doc, "Rendering Top", image_paths[0], "Figure 1: Rendering Top")
replace_image(doc, "Rendering Bottom",
              image_paths[1], "Figure 2: Rendering Bottom")

for key, value in parser.stackup.items():
    if key == "copper_finish":
        edit_table_field(doc, "Copper Finish", value)
    if key == "F.Cu":
        edit_table_field(doc, "Copper Top", int(
            float(value["thickness"])*1000))
    elif key == "B.Cu":
        edit_table_field(doc, "Copper Bottom", int(
            float(value["thickness"])*1000))
    elif key == "In1.Cu":
        edit_table_field(doc, "Copper In.1", int(
            float(value["thickness"])*1000))
    elif key == "In2.Cu":
        edit_table_field(doc, "Copper In.2", int(
            float(value["thickness"])*1000))

    elif key == "F.Mask":
        if pcb_type == "FPC":
            edit_table_field(doc, "Polyimide (external)",
                             int(float(value["thickness"])*1000))
        else:
            edit_table_field(doc, "Solder resist Top",
                             int(float(value["thickness"])*1000))
    elif key == "B.Mask":
        if pcb_type == "FPC":
            edit_table_field(doc, "Polyimide (external)",
                             int(float(value["thickness"])*1000))
        else:
            edit_table_field(doc, "Solder resist Bottom",
                             int(float(value["thickness"])*1000))

    elif key == "dielectric 1":
        if pcb_type == "FPC":
            edit_table_field(doc, "Polyimide (internal)", int(
                float(value["thickness"])*1000))
        elif pcb_type == "4L":
            edit_table_field(doc, "Prepreg", int(
                float(value["thickness"])*1000))
        elif pcb_type == "2L":
            edit_table_field(doc, "Core", int(
                float(value["thickness"])*1000))

    elif key == "dielectric 2":
        edit_table_field(doc, "Core", int(
            float(value["thickness"])*1000))
    elif key == "dielectric 3":
        edit_table_field(doc, "Prepreg", int(
            float(value["thickness"])*1000))

# Save the modified document back to the same file as .docx
doc.save(
    f"QM/{sys.argv[1]} Gerber and PCB Datasheet/PCB Datasheet {sys.argv[1]}.docx")
print(f"Datasheet created successfully: QM/"
      f"{sys.argv[1]} Gerber and PCB Datasheet/PCB Datasheet "
      f"{sys.argv[1]}.docx")
