#!/bin/sh
# Echo commands and exit on error
set -x
set -e

cd "$CI_PROJECT_DIR/${KICAD_PROJECT}"
echo "Changed to project directory: $(pwd)"

# Find and print pcb layout and create gerbers and drill files
if [ -n "$(find "." -maxdepth 1 -name '*.kicad_pcb' -print -quit)" ]; then
    if [ -n "$(find "." -maxdepth 1 -name '*.json' -print -quit)" ]; then
        # get panel file
        PANEL_FILE=$(find "../QM/${KICAD_PROJECT} Panel" -maxdepth 2 -name '*.kicad_pcb' -print -quit)
        kibot -c ../kicad-automation/export/config.kiplot.yaml -b "$PANEL_FILE" -d "../QM/${KICAD_PROJECT} Gerber and PCB Datasheet" gerber drill netlist
        echo "Exported gerbers and drill files of ${KICAD_PROJECT}"
    else
        kibot -c ../kicad-automation/export/config.kiplot.yaml -d "../QM/${KICAD_PROJECT} Gerber and PCB Datasheet" gerber drill netlist
        echo "Exported gerbers and drill files of ${KICAD_PROJECT}"
    fi
else
  echo "No .kicad_pcb files found in the project directory"
  exit 1
fi

cd "../QM/${KICAD_PROJECT} Gerber and PCB Datasheet/Gerber"
ls -l
rm -f *Fab.gbr
rm -f *Margin.gbr
rm -f *Courtyard.gbr
ls -l
