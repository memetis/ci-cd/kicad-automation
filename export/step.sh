#!/bin/sh
# Echo commands and exit on error
set -x
set -e

cd "$CI_PROJECT_DIR/${KICAD_PROJECT}"
echo "Changed to project directory: $(pwd)"

# Find and print pcb layout and creete 3D render
if [ -n "$(find "." -maxdepth 1 -name '*.kicad_pcb' -print -quit)" ]; then
  kibot -c ../kicad-automation/export/config.kiplot.yaml -d "../QM" step
  echo "Printed 3D rendering of ${KICAD_PROJECT}"
else
  echo "No .kicad_pcb files found in the project directory"
  exit 1
fi
