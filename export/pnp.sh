#!/bin/sh
# Echo commands and exit on error
set -x
set -e

cd "$CI_PROJECT_DIR/${KICAD_PROJECT}"
echo "Changed to project directory: $(pwd)"

# Find and print pcb layout and create gerbers and drill files
if [ -n "$(find "." -maxdepth 1 -name '*.kicad_pcb' -print -quit)" ]; then
  kibot -c ../kicad-automation/export/config.kiplot.yaml -d "../QM/${KICAD_PROJECT} Gerber and PCB Datasheet/Assembly" pnp
  echo "Exported gerbers and drill files of ${KICAD_PROJECT}"
else
  echo "No .kicad_pcb files found in the project directory"
  exit 1
fi

