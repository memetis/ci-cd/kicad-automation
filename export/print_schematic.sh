#!/bin/sh
# Echo commands and exit on error
set -x
set -e

cd "$CI_PROJECT_DIR/${KICAD_PROJECT}"
echo "Changed to project directory: $(pwd)"

# Find and print schematic
if [ -n "$(find "." -maxdepth 1 -name '*.kicad_sch' -print -quit)" ]; then
  kibot -c ../kicad-automation/export/config.kiplot.yaml -d "../QM" print_schematic
  echo "Printed schematic of ${KICAD_PROJECT}"
else
  echo "No .kicad_sch files found in the project directory"
  exit 1
fi